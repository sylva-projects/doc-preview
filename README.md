# Project for Documentation Preview

The main repo of Sylva documentation is https://gitlab.com/sylva-projects/sylva-projects.gitlab.io,
which publish the content on the URL: https://sylva-projects.gitlab.io/

This project provides "Online Preview" for open MRs in the main doc repo:  

In the [main repo pipeline](https://gitlab.com/sylva-projects/sylva-projects.gitlab.io/-/pipelines), we can manually trigger the "Preview" step, 

![](./img/pipeline.png)

This project will clone the working branch and rendered it at the "Preview" URL https://sylva-projects.gitlab.io/doc-preview/
